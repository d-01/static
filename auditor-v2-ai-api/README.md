# AI API

## Эндпоинты

- `/api/v1/projects/document/text_continue/` → `/api/v1/ai/text_continue/`
  - Продолжение заданного текста. Исходный текст не содержится в ответе.
- `/api/v1/projects/document/text_generator/` → `/api/v1/ai/theme_to_text/`
  - Генерация абзаца (ок. 60 слов) на заданную тему.
- `/api/v1/projects/document/text_rephrase/` → `/api/v1/ai/text_rephrase/`
  - Генерация шести вариантов входного текста со схожим смыслом, другими словами.
  - Возвращает массив.
- `/api/v1/projects/document/text_shorter/` → `/api/v1/ai/text_shorter/`
  - Делает входной текст короче, сохраняя смысл.
- new: `/api/v1/ai/query_ai/`
  - Входная строка-команда `source` передается языковой модели как есть (без изменений).
  - Дополнительно можно передать строку-контекст `context`, которая будет помещена *перед* командой.

## Формат ответа

Поля:

- `payload` - содержит ответ в готовом для использования виде (строка или список строк) или `null` в случае ошибки.
- `error` - содержит `null` или строку, описание ошибки без подробностей.
- (служебное поле) `debugInfo` - присутствует только в режиме `DEBUG=True`. Содержит информацию для отладки.



Пример запроса к `/api/v1/ai/query_ai/`:

```json
{
  "source": "кошка",
  "context": null
}
```



Пример ответа:

```json
{
  "payload": "Кошка - это домашнее животное, ...",
  "error": null,
  "debugInfo": {
    "messages": [
      {
        "role": "user",
        "content": "кошка"
      }
    ],
    "config": {
      "model": "gpt-3.5-turbo",
      "maxTokens": 1500,
      "temperature": 0,
      "topP": 1,
      "stream": false,
      "stop": null,
      "presencePenalty": 0,
      "frequencyPenalty": 0,
      "logitBias": {}
    },
    "response": {
      "id": "chatcmpl-6xrzCkWd6EmzlBKxcuQmd7yfXgDNZ",
      "object": "chat.completion",
      "created": 1679726894,
      "model": "gpt-3.5-turbo-0301",
      "usage": {
        "promptTokens": 12,
        "completionTokens": 200,
        "totalTokens": 212
      },
      "choices": [
        {
          "message": {
            "role": "assistant",
            "content": "Кошка - это домашнее животное, ..."
          },
          "finishReason": "stop",
          "index": 0
        }
      ],
      "responseMs": 9806
    },
    "errorDetails": null
  }
}
```





